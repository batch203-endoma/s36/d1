const express = require("express");
//express.router() method allows access to HTTP methods
const router = express.Router();

const taskControllers = require("../controllers/taskControllers.js");


//Create task routes
router.post("/", taskControllers.createTask);

//View all tasks routes
router.get("/allTasks", taskControllers.getAllTasksController);

//Get a single tasks
router.get("/getSingleTask/:taskId", taskControllers.getSingleTaskController);

//update a task status
router.patch("/updateTask/:taskId", taskControllers.updateTaskStatusController);

//Delete a task
router.delete("/deleteTask/:taskId", taskControllers.deleteTaskController);


//this will be use in our server.
module.exports = router;
