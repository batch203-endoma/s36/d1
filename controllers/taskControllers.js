const Task = require("../models/Task.js");

// Syntax app.httpMethods ("/endpoint", (anonymousFunc));

// Create task:create a task without duplicate names.
// console.log(controllers);
module.exports.createTask = (req, res) => {
  //checking captured data from the request body.
  console.log(req.body);
  Task.findOne({
      name: req.body.name
    }).then(result => {
      console.log(result);
      if (result != null && result.name == req.body.name) {
        return res.send(`Duplicate task found`)
      } else {
        let newTask = new Task({
          name: req.body.name
        })
        newTask.save()
          .then(result => res.send(result))
          .catch(error => res.send(error));
      }
    })
    .catch(error => res.send(error))
}

//Retrieve All task
  module.exports.getAllTasksController = (req, res) => {
    Task.find({})
    .then(tasks => res.send(tasks))
    .catch(error => res.send(error));
  }

//Retrieve a single task
module.exports.getSingleTaskController = (req, res) => {
  console.log(req.params);
  Task.findById(req.params.taskId)
  .then(result => res.send(result))
  .catch(error => res.send(error));
}

//Updating task status

module.exports.updateTaskStatusController = (req, res) => {
  console.log(req.params.taskId);
  console.log(req.body);

  let updates = {
    status:req.body.status
  };

  // Syntax: findByIdAndUpdate("_ID", {objectUpdate}, options )
    // {new:true} - returns the updated verions of the document.
  Task.findByIdAndUpdate(req.params.taskId, updates, {new:true})
  .then(updatedTask => res.send(updatedTask))
  .catch(err => res.send(err));
}



//Deleting a Task
module.exports.deleteTaskController = (req, res) => {
  console.log(req.params.taskId);
  Task.findByIdAndRemove(req.params.taskId)
  .then(removedTask => res.send(removedTask))
  .catch(err => res.send(err));
}
















//
