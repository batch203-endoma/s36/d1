const mongoose =require("mongoose");
const taskSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Task name is required"]
  },
  status: {
    type: String,
    default: "pending"

  }
})
//modeule.exports will allows us to export file/functions and and be able to import/require them in another file within our application.
// files/function of Task.js will be used in the controllers folder.
module.exports = mongoose.model("Task", taskSchema);
